//
//  AppDelegate.h
//  MesAg
//
//  Created by Илья Пупкин on 10/19/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

