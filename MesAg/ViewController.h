//
//  ViewController.h
//  MesAg
//
//  Created by Илья Пупкин on 10/19/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MATableViewController.h"
#import <VK-ios-sdk/VKSdk.h>

@interface ViewController : UIViewController <VKSdkDelegate>

@property (weak, nonatomic) IBOutlet UIButton *vkLoginButton;

- (void) vkLogin;

@end

