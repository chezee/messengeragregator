//
//  MAMenuCell.h
//  MesAg
//
//  Created by Илья Пупкин on 10/20/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAMenuCell : UITableViewCell

-(void) configureWithItem:(NSDictionary *) item;

@end
