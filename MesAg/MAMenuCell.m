//
//  MAMenuCell.m
//  MesAg
//
//  Created by Илья Пупкин on 10/20/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MAMenuCell.h"

@implementation MAMenuCell


- (void) configureWithItem:(NSMutableDictionary *)item
{
    NSLog(@"Item for cell: %@", item);
}

@end
