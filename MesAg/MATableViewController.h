//
//  MATableViewController.h
//  MesAg
//
//  Created by Илья Пупкин on 10/19/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MATableViewController : UITableViewController

@property (nonatomic, strong) IBOutlet UITableView *table;

@end
