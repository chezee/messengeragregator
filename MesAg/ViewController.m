//
//  ViewController.m
//  MesAg
//
//  Created by Илья Пупкин on 10/19/16.
//  Copyright © 2016 Ilya Gorevoy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    FBSDKLoginButton *fbLoginButton = [[FBSDKLoginButton alloc] initWithFrame:CGRectMake((self.view.center.x - 60), self.view.center.y, 120, 50)];
    fbLoginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    
    [self.vkLoginButton addTarget:self action:@selector(vkLogin) forControlEvents:UIControlEventTouchDown];
    self.vkLoginButton.frame = CGRectMake((self.view.center.x - 60), (self.view.center.y - 60), 120, 50);
    
    [self.view addSubview:fbLoginButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fbLoggedIn];
    });
    NSLog(@"Token is: %@", [FBSDKAccessToken currentAccessToken]);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    
  
}

- (void) vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if(result.token)
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MATableViewController"]
                           animated:YES
                         completion:nil];
    else if(result.error)
        NSLog(@"%@", result.error);
}

- (void) vkSdkUserAuthorizationFailed
{
    
}

- (void) vkLogin
{
    [[VKSdk initializeWithAppId:@"5713645"] registerDelegate:self];
    [VKSdk authorize:@[@"email"]];
    [VKSdk accessToken];
}

- (void) fbLoggedIn
{
        if ([FBSDKAccessToken currentAccessToken]) {
            NSLog(@"FBToken: %@", [FBSDKAccessToken currentAccessToken]);
            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MATableViewController"]
                               animated:YES
                             completion:nil];
        }
        else
            NSLog(@"No FB token");
        
}
@end
